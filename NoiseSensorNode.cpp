// vim: noai:sw=2:tw=88

#include "NoiseSensorNode.h"
#include "NoiseSensor.h"

NoiseSensorNode::NoiseSensorNode() {};

NoiseSensorNode::NoiseSensorNode(uint8_t node_id, uint8_t size) {
  this->_node_id = node_id;
  this->_measurements = new uint16_t[size];
} 
