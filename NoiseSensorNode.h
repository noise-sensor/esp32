// vim: noai:sw=2:tw=88

#pragma once

#include <Arduino.h>

class NoiseSensorNode {
  private:
    uint8_t   _node_id;

  public:
    uint32_t  _sync_message_ts;
    uint16_t* _measurements;  // For a dynamic array later.    
    String    sensebox_id;
    String    sensor_id;    
    uint8_t   missing_answers;
    bool      _battery_low = 0;

    NoiseSensorNode();
    NoiseSensorNode(uint8_t node_id, uint8_t size);
};
