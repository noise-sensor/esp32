// vim: noai:sw=2:tw=88

#include <Wire.h>
#include "NoiseSensorNode.h"

#include <WiFiUdp.h>

#define LORA_HEADER_LENGTH 2
#define BATTERY_FIELD_SIZE 1
#define PACKET_COUNT 5

class NoiseSensorClass {
  private:
    //NTPClient*        _time_client;
    uint32_t          _last_sync_time;
    uint8_t           _tick_count         = 20;
    uint8_t           _tick_duration      = 2;    // in seconds
    uint8_t           _tx_offset          = 1;
    uint8_t           _i2c_address        = 0x3C;
    bool              _first_sync         = true;

    // These aren't _real_ const but they shouldn't be redefined nonetheless.
    uint16_t          _PACKET_SIZE;
    uint16_t          _REQUEST_SIZE;
    
    NoiseSensorNode*  _nodes[];

    uint8_t         getDeviceID();
    bool            setupNTPClient();
    uint32_t        getEpoch();


  public:
    /**
     * Begin the measurement cycle by sending a sync message.
     *
     * @return true on success, false on failure
     */
    bool begin();

    /**
     * Call begin() with a custom tick count.
     *
     * @return true on success, false on failure
     */
    bool begin(uint8_t tick_count);

    /**
     * Call begin() with a custom tick count and tick duration.
     *
     * @return true on success, false on failure
     */
    bool begin(uint8_t tick_count, uint8_t tick_duration);

    /**
     * Call begin() with a custom tick count, tick duration and tx_offset.
     *
     * @return true on success, false on failure
     */
    bool begin(uint8_t tick_count, uint8_t tick_duration, uint8_t tx_offset);

    void setIds(const char* sensebox_ids[], const char* sensor_ids[]);
    String buildHTTPHeader(uint8_t device_id, const char* server, uint16_t content_length);

    bool beaconReady();
    bool requestReady();

    /**
     * Read the newly available data from the slave.
     *
     * @return true on success, false on failure
     */
    bool read();

    /**
     * Print the measurements in a pretty format.
     */
    void printMeasurements(uint8_t device_id);

    /** 
     * Send the sync message to the slave.
     *
     * @return true on success, false on failure
     */
    bool sendSyncBeacon();
    
    String buildSenseBoxJSON(uint8_t device_id);

};

extern NoiseSensorClass NoiseSensor;
